#!/bin/bash

# simple script to start the rhizome uwsgi server
# !! DONOT run as ROOT !!
source /home/hsn/.envs/rhizome/bin/activate
uwsgi --uid=500 --gid=500 /home/hsn/rhizome/rhizome/uwsgi.ini