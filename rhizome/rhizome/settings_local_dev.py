from pymongo import MongoClient


VM_HOST_IP = 'localhost'

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'rhizome_dev',  # Or path to database file if using sqlite3.
        'USER': 'rhizome-dev',  # Not used with sqlite3.
        'PASSWORD': 'rhizome-5678',  # Not used with sqlite3.
        'HOST': VM_HOST_IP,  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',  # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {
            'init_command': 'SET storage_engine=INNODB',
        }
    }
}


MONGODB_DATABASES = {
    "default": {
        "name": 'pr47_dev',
        "host": VM_HOST_IP,
        "tz_aware": True,  # if you using timezones in django (USE_TZ = True)
    },
}
MONGO_CLIENT = MongoClient(VM_HOST_IP, 27017)


BROKER_URL = 'redis://' + VM_HOST_IP + ':6379/0'
CELERY_RESULT_BACKEND = 'redis://' + VM_HOST_IP + ':6379/1'