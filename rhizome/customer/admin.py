# Register your models here.
from django.contrib import admin
from customer.models import Customer


class CustomerAdmin(admin.ModelAdmin):

    list_display = ('guid', 'name', 'mobile', 'email', 'created_at', 'updated_at', 'mobile_user_id')

    search_fields = ['name', 'mobile', 'email', 'mobile_user_id']

admin.site.register(Customer, CustomerAdmin)