from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import render, redirect

# Create your views here.


class LoginView(View):
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'internal/login.html', context)


class SignupView(View):
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'internal/registration.html', context)


class VideoView(View):
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'internal/video_player.html', context)


def index_view(request):
    context = {}
    return render(request, 'internal/index.html', context)
