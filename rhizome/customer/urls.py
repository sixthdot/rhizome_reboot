from django.conf import settings
from django.contrib import admin
from django.conf.urls import include, url
from django.conf.urls.static import static
from customer.views import LoginView, SignupView, VideoView


urlpatterns = [

    url(r'^login/$', LoginView.as_view(), name='login_view'),
    url(r'^signup/$', SignupView.as_view(), name='signup_view'),
    url(r'^video/$', VideoView.as_view(), name='video_view'),
]
