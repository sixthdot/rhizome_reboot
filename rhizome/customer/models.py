from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
import uuid
import logging

logger = logging.getLogger(__name__)

class Customer(models.Model):

    guid =  models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    user = models.OneToOneField(User, db_index=True, related_name="customer")
    name = models.CharField(max_length=200, null=True, blank=True)
    mobile = models.CharField(max_length=15,  null=True, blank=True)
    email = models.EmailField(max_length=300, null=True, blank=False, db_index=True)
    org  = models.CharField(max_length=15,  null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # mobile user id used for push notifications and identifying users
    mobile_user_id = models.CharField(max_length=200, null=True, blank=True)
    mobile_device_id = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):

        return "%s<---->%s" %(self.guid, self.email)


# post save for creating customer on user saving
def create_customer(sender, instance, created, **kwargs):
    
    # create customer instance 
    if created:

        customer, created = Customer.objects.get_or_create(user=instance, email=instance.email)

        # create token for user for mobile APIs
        
        user = customer.user
        token = Token.objects.create(user=user)
        logger.info("Created token for user %s : %s" %(user, token))
    

post_save.connect(create_customer, sender=User)