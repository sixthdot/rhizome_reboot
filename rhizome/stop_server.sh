#!/bin/bash

# simple bash file to stop rhizome uwsgi server
# should be executed as the same user that was used to start the server
source /home/hsn/.envs/rhizome/bin/activate
uwsgi --uid=500 --gid=500 --stop /tmp/uwsgi.pid # path to the uwsgi process pid file as mentioned in uwsgi.ini