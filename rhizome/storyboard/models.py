from __future__ import unicode_literals

from django.db import models
import logging
import uuid
from customer.models import Customer
from django.conf import settings
import os
from django.core.urlresolvers import reverse

logger = logging.getLogger(__name__)


class ExperienceTemplate(models.Model):

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    name = models.CharField(max_length=255, unique=True, null=False, blank=False)
    description = models.TextField(blank=True)
    meta_file = models.CharField(max_length=255, null=False, blank=False)
    web_template = models.CharField(max_length=255, null=True, blank=True)
    vr_template = models.CharField(max_length=255, null=True, blank=True)
    mobile_template = models.CharField(max_length=255, null=True, blank=True)
    embed_template = models.CharField(max_length=255, null=True, blank=True)
    thumbnail = models.URLField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s<---->%s" % (self.guid, self.name)

    def get_meta_file_path(self):
        return os.path.join(settings.BASE_DIR, self.meta_file)


class ExperienceTemplateCollection(models.Model):

    experience_template = models.ForeignKey(ExperienceTemplate)
    collection = models.CharField(max_length=200, null=False, blank=False)

    def __unicode__(self):
        return "%s:%s <--> %s" % (self.experience_template.guid, self.experience_template.name, self.collection)


class CustomerStoryboard(models.Model):

    DRAFT = 'DR'
    PUBLISHED = 'PB'
    ARCHIVED = 'AR'
    DELETED = 'DL'
    PUB_STATES = (
        (DRAFT, 'Draft'),
        (PUBLISHED, 'Published'),
        (ARCHIVED, 'Archived'),
        (DELETED, 'Deleted'),
    )

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    name = models.CharField(max_length=255, null=False, blank=False)
    current_state = models.CharField(max_length=2,
                                     choices=PUB_STATES,
                                     default=DRAFT)
    experience_template = models.ForeignKey(ExperienceTemplate)
    customer = models.ForeignKey(Customer)
    storyboard_id = models.CharField(max_length=255, null=False, blank=False)
    is_private = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ["-created_at"]

    def __unicode__(self):
        return "%s:%s <--> %s <--> %s" % (self.guid, self.name, self.customer, self.current_state)

    def get_absolute_url(self):
        return reverse('storyboard_url:render_storyboard_web', args=[str(self.guid)])
