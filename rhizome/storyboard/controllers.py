import logging
from django.conf import settings
from jinja2 import Environment, FileSystemLoader
import json

logger = logging.getLogger(__name__)
env = Environment(loader=FileSystemLoader(settings.STORYBOARD_TEMPLATES_PATH))


class StoryboardController:

    @staticmethod
    def get_test_storyboard():
        """ generate a test storyboard """

        templ = env.get_template("test.j2")
        resp_string = templ.render(baseColor="#A4AB4B", title="Knowlarity",
                                   logoUrl="https://static-assets.knowlarity.com/wp-content/uploads/2014/04/logo.png",
                                   tagline="GIVING BUSINESS A VOICE",
                                   templateId=2)
        return json.loads(resp_string)
