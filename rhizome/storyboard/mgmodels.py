from django.conf import settings
import logging
import uuid
import datetime
from bson.objectid import ObjectId

logger = logging.getLogger(__name__)
sb_db = settings.MONGO_CLIENT.storyboard_db


class Storyboard:

    def __init__(self, storyboard=None, title=None, logo=None, baseColor=None, template_id=None):
        self.sb_db = sb_db
        self.sb_collection = self.sb_db.storyboards

        if storyboard is not None:
            self.storyboard = storyboard
            self.id = self.storyboard['_id']
            self.storyboard_id = str(self.id)

        else:
            self.title = title
            self.logo = logo
            self.template_id = template_id
            self.objects = {}
            self.mappings = {}
            self.storyboard = None
            self.id = None
            self.storyboard_id = None

    def add_text(self, data):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'TEXT',
            'data': data
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_image(self, url, thumbnail, width, height):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'IMAGE',
            'url': url,
            'thumbnail': thumbnail,
            'width': width,
            'height': height,
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_video(self, url, thumbnail, width, height, autoplay=False):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'VIDEO',
            'url': url,
            'thumbnail': thumbnail,
            'width': width,
            'height': height,
            'autoplay': autoplay,
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_audio(self, url, thumbnail, title=None, autoplay=False):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'VIDEO',
            'url': url,
            'thumbnail': thumbnail,
            'title': title,
            'autoplay': autoplay,
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_feed(self, feedUrl, feedTitle, feedDescription, thumbnail):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'FEED',
            'feedUrl': url,
            'feedTitle': feedTitle,
            'feedDescription': feedDescription,
            'thumbnail': thumbnail,
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_geo(self, lat, lng, url, title, description, thumbnail=None):
        obj_id = str(uuid.uuid4())
        obj_hash = {
            'objectType': 'GEO',
            'url': url,
            'lat': lat,
            'lng': lng,
            'title': title,
            'description': description,
            'thumbnail': thumbnail,
        }
        self.objects[obj_id] = obj_hash
        return obj_id

    def add_mapping(self, dropzone, dropzoneType, objects):
        self.mappings[dropzone] = {
            'dropzoneType': dropzoneType,
            'objects': objects,
        }

    def __generate(self):
        self.storyboard = {
            'title': self.title,
            'logo': self.logo,
            'templateId': self.template_id,
            'objects': self.objects,
            'mappings': self.mappings,
            'createdAt': datetime.datetime.utcnow(),
        }
        return self.storyboard

    def save(self):
        """ save the json into mongo """

        self.__generate()
        self.id = self.sb_collection.insert_one(self.storyboard).inserted_id
        self.storyboard_id = str(self.id)
        return self.storyboard_id

    def delete(self):
        """ delete the storyboard """

        if self.id is not None:
            self.sb_collection.delete_one({'_id': self.id})
            self.id = None
            self.storyboard_id = None
            self.storyboard = None
            return True
        return False

    def update(self):
        pass

    @staticmethod
    def get(storyboard_id):
        """ load back the storyboard json """

        storyboard = sb_db.storyboards.find_one({"_id": ObjectId(storyboard_id)})
        if storyboard:
            sb_obj = Storyboard(storyboard=storyboard)
            return sb_obj
        return None
