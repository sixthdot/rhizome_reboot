from django.contrib import admin
from storyboard.models import ExperienceTemplate, ExperienceTemplateCollection, CustomerStoryboard


class ExperienceTemplateAdmin(admin.ModelAdmin):

    list_display = ('guid', 'name', 'meta_file', 'web_template', 'created_at', 'updated_at',)
    search_fields = ['name', 'web_template', ]


class ExperienceTemplateCollectionAdmin(admin.ModelAdmin):

    list_display = ('id', 'experience_template', 'collection',)
    search_fields = ['experience_template', 'collection', ]


class CustomerStoryboardAdmin(admin.ModelAdmin):

    list_display = ('guid', 'name', 'customer', 'experience_template', 'current_state', 'created_at', 'updated_at',)
    search_fields = ['name', 'customer', ]


admin.site.register(ExperienceTemplate, ExperienceTemplateAdmin)
admin.site.register(ExperienceTemplateCollection, ExperienceTemplateCollectionAdmin)
admin.site.register(CustomerStoryboard, CustomerStoryboardAdmin)
