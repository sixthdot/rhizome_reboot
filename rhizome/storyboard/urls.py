from django.conf import settings
from django.contrib import admin
from django.conf.urls import include, url
from django.conf.urls.static import static
from storyboard.api import TestStoryboard, StoryboardApi
from storyboard.views import render_storyboard_web
from storyboard.views import ExperienceTemplateList, ExperienceWizardView
from storyboard.views import PublishWizardView, StoryboardListView

urlpatterns = [

    url(r'^test/$', TestStoryboard.as_view(), name='test_storyboard'),
    url(r'^create/step1/$', ExperienceTemplateList.as_view(), name='experience_template_list_view'),
    url(r'^create/step2/$', ExperienceWizardView.as_view(), name='experience_wizard_view'),
    url(r'^share/(?P<sid>[a-zA-Z0-9\-]+)/$', PublishWizardView.as_view(), name='publish_wizard_view'),
    url(r'^list/$', StoryboardListView.as_view(), name='storyboard_list_view'),
    url(r'^(?P<csb_guid>[a-zA-Z0-9\-]+)/$', render_storyboard_web, name='render_storyboard_web'),
    url(r'^_api/(?P<csb_guid>[a-zA-Z0-9\-]+)/$', StoryboardApi.as_view(), name='storyboard_api_view'),
]
