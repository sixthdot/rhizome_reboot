from django.shortcuts import render, redirect
from storyboard.mgmodels import Storyboard
from storyboard.models import ExperienceTemplate, CustomerStoryboard
from django.views.generic import ListView
from django.views.generic import View
from django.shortcuts import get_object_or_404
import json
from customer.models import Customer
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.sites.models import Site
import logging

logger = logging.getLogger(__name__)


def context_mapper(storyboard):
    """ creates an easily parsable context object from storyboard, mostly to
        ease handling in django templates """

    ret_hash = {}

    for dzname in storyboard['mappings'].keys():
        ret_hash[dzname] = {
            'dropzoneType': storyboard['mappings'][dzname]['dropzoneType'],
            'data': [storyboard['objects'][obj_id] for obj_id in storyboard['mappings'][dzname]['objects']]
        }
    return ret_hash


def render_storyboard_web(request, csb_guid):
    """ display the storyboard according to the template specified
        TODO: fix the way template file is picked up. Perhaps provide it in settings.
    """

    csb = get_object_or_404(CustomerStoryboard, guid=csb_guid)
    sb = Storyboard.get(csb.storyboard_id)

    if sb:
        context = context_mapper(sb.storyboard)
        return render(request, csb.experience_template.web_template, context)
    else:
        raise Http404("Storyboard does not exist")


class ExperienceTemplateList(LoginRequiredMixin, ListView):

    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    model = ExperienceTemplate
    template_name = 'internal/create_experience_step1.html'


class ExperienceWizardView(LoginRequiredMixin, View):

    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        """ returns the template meta information to generate the wizard """

        exp_model = get_object_or_404(ExperienceTemplate, guid=request.GET['tid'])
        with open(exp_model.get_meta_file_path()) as f:
            templ_meta = json.loads(f.read())
        context = {
            'template_meta': templ_meta,
            'experience_template': exp_model
        }
        return render(request, 'internal/create_experience_step2.html', context)

    def post(self, request, *args, **kwargs):
        """ creates the storyboard based on dropzone values entered by the user """

        exp_model = get_object_or_404(ExperienceTemplate, guid=request.GET['tid'])
        with open(exp_model.get_meta_file_path()) as f:
            templ_meta = json.loads(f.read())

        title = request.POST.get('title')
        logo = request.POST.get('logo')

        sb_obj = Storyboard(title=title, logo=logo, template_id=exp_model.guid)

        for dzname in templ_meta['dropzones'].keys():

            if templ_meta['dropzones'][dzname]['dropzoneType'] == 'TEXT':
                obj_id = sb_obj.add_text(request.POST.get(dzname))
                sb_obj.add_mapping(dzname, 'TEXT', [obj_id])

            elif templ_meta['dropzones'][dzname]['dropzoneType'] == 'IMAGE':
                obj_id = sb_obj.add_image(url=request.POST.get(dzname), thumbnail=request.POST.get(dzname), width=500,
                                          height=300)
                sb_obj.add_mapping(dzname, 'IMAGE', [obj_id])

            elif templ_meta['dropzones'][dzname]['dropzoneType'] == 'IMAGE-GROUP':
                image_urls = request.POST.get(dzname).split(',')
                obj_arr = []
                for url in image_urls:
                    obj_id = sb_obj.add_image(url=url, thumbnail=url, width=500, height=300)
                    obj_arr.append(obj_id)
                sb_obj.add_mapping(dzname, 'IMAGE', obj_arr)

            elif templ_meta['dropzones'][dzname]['dropzoneType'] == 'VIDEO-GROUP':
                video_urls = request.POST.get(dzname).split(',')
                obj_arr = []
                for url in video_urls:
                    obj_id = sb_obj.add_video(url=url, thumbnail=url, width=500, height=300)
                    obj_arr.append(obj_id)
                sb_obj.add_mapping(dzname, 'VIDEO', [obj_id])

        print sb_obj.storyboard
        sb_obj.save()

        if sb_obj.storyboard_id is not None:
            customer = Customer.objects.filter()[0]
            customer_sb = CustomerStoryboard(name=title, experience_template=exp_model, customer=customer,
                                             storyboard_id=sb_obj.storyboard_id)
            customer_sb.save()
            return redirect('storyboard_url:publish_wizard_view', sid=customer_sb.guid)

        context = {
            'template_meta': templ_meta,
            'experience_template': exp_model
        }
        return render(request, 'internal/create_experience_step2.html', context)


class PublishWizardView(LoginRequiredMixin, View):

    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        """ handles the final step of the experience creation process """

        s_guid = kwargs['sid']
        sb = get_object_or_404(CustomerStoryboard, guid=s_guid)

        context = {
            'storyboard': sb
        }
        return render(request, 'internal/create_experience_step3.html', context)

    def post(self, request, *args, **kwargs):
        """ sends emails to a list of email addresses provided
            TODO: move emails to async task. Verify email addresses. Perhaps put a form validation.
        """

        s_guid = kwargs['sid']
        sb = get_object_or_404(CustomerStoryboard, guid=s_guid)

        if 'emails' in request.POST:
            emails = request.POST.get('emails').split(',')
            for email in emails:
                subj = "Check out " + sb.name
                body = "A sixthdot experience has been privately shared with you. You can see it at http://%s%s" % (Site.objects.get_current().domain, sb.get_absolute_url())
                if 'email_message' in request.POST and len(request.POST.get('email_message')) > 0:
                    body = body + "\nThe sender added the following private note:\n"
                    body = body + request.POST.get('email_message')
                send_mail(subj, body, 'noreply@viewfieldlabs.net', [email])
            print request.POST.get('email_message')

        context = {
            'storyboard': sb
        }
        return render(request, 'internal/create_experience_step3.html', context)


class StoryboardListView(LoginRequiredMixin, ListView):

    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    model = CustomerStoryboard
    template_name = 'internal/mystoryboards.html'
