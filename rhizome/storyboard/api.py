from django.shortcuts import render, get_object_or_404
import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import AllowAny
from storyboard.controllers import StoryboardController
from storyboard.models import CustomerStoryboard
from storyboard.mgmodels import Storyboard
from storyboard.views import context_mapper


logger = logging.getLogger(__name__)


class TestStoryboard(APIView):
    """
    Returns a test storyboard json.
    Doesn't require any parameters.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        """
        Return a test storyboard.
        """
        return Response(StoryboardController.get_test_storyboard())


class StoryboardApi(APIView):
    """
    Returns the storyboard JSON, based on the ID provided.
    """
    permission_classes = (AllowAny,)

    def get(self, request, csb_guid, format=None):

        csb = get_object_or_404(CustomerStoryboard, guid=csb_guid)
        sb = Storyboard.get(csb.storyboard_id)

        if sb:
            context = context_mapper(sb.storyboard)
            return Response(context)

        return Response({'success': False})