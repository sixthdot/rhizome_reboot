from __future__ import unicode_literals
import datetime
from django_mongoengine import Document, EmbeddedDocument, fields


class Channel(Document):
    title = fields.StringField(max_length=255, required=True)
    region = fields.StringField(max_length=255, required=True)
    description = fields.StringField(max_length=1000, required=False)
    username = fields.StringField(max_length=255, default='rhizome', required=False)
    feed_list = fields.ListField(blank=True)
    author_list = fields.ListField(blank=True)
    entity_list = fields.ListField(blank=True)

    def __unicode__(self):
        return self.title


class Feed(Document):
    title = fields.StringField(max_length=255, required=True)
    url = fields.URLField(max_length=1000, required=True)
    last_processed = fields.DateTimeField(blank=True)
    if_valid = fields.BooleanField(default=False, required=True)

    def __unicode__(self):
        return self.title


class Article(Document):
    title = fields.StringField(max_length=255, required=True)
    summary = fields.StringField()
    url = fields.URLField(max_length=1000, required=True)
    top_image = fields.StringField(max_length=255, required=True)
    author = fields.ObjectIdField(max_length=255, required=False)
    publish_date = fields.DateTimeField()
    entity_list = fields.ListField(blank=True)
    feed_id = fields.ObjectIdField(max_length=255, required=True)

    def __unicode__(self):
        return self.title


class Entity(Document):
    title = fields.StringField(max_length=1000, required=True)
    score = fields.IntField(default=1, required=True)
    channel_id = fields.ObjectIdField(max_length=255, required=True)

    def __unicode__(self):
        return self.title

    meta = {
        'allow_inheritance': True,
        'ordering': ['title']
    }


class Author(Document):
    name = fields.StringField(max_length=255, required=True)
    email = fields.EmailField(max_length=255, required=False)
    phone = fields.StringField(max_length=255, required=False)
    address = fields.StringField(max_length=1000, required=False)
    current_publication = fields.StringField(max_length=255, required=False)
    designation = fields.StringField(max_length=255, required=False)
    profile_image = fields.StringField(max_length=255, required=False)
    press_club_city = fields.StringField(max_length=255, required=False)
    created_at = fields.DateTimeField(default=datetime.datetime.now, required=True, editable=False,)
    articles = fields.ListField(blank=True)
    entities = fields.ListField(blank=True)
    channels = fields.ListField(blank=True)
    publications = fields.ListField(blank=True)

    def __unicode__(self):
        return self.name

    meta = {
        'allow_inheritance': True,
        'indexes': ['name', 'email', 'phone', 'current_publication',],
        'ordering': ['name']
    }
