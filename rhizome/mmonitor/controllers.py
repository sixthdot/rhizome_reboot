from models import Channel
import feedparser
from extractor import Extractor
from models import Article, Entity, Feed, Author
from datetime import datetime
import logging

logger = logging.getLogger(__name__)



class ChannelController:

    @staticmethod
    def get_list_of_channels(prefix_str=None):
        """ get list of all channels in the system """

        if prefix_str is None:
            channel_list = Channel.objects
        else:
            channel_list = Channel.objects(prefix_str__istartswith=prefix_str)

        return channel_list

    @staticmethod
    def add(title, description, region):
        """ add a new channel """

        ch = Channel(title=title, description=description, region=region)
        ch.save()
        return ch


class AuthorController:

    @staticmethod
    def add(name, email=None, phone=None, address=None, current_publication=None,
            designation=None, profile_image=None, press_club_city=None):
        """ add new author """

        author_list = Author.objects(name__iexact=name)
        if author_list.count() > 0:
            author = author_list.first()

        else:
            author = Author(name, email, phone, address, current_publication,
                            designation, profile_image, press_club_city)

        if current_publication not in author.publications:
            author.publications.append(current_publication)

        author.save()
        return author


class FeedController:

    @staticmethod
    def add(url, channel):
        """ add a new feed """

        fds = Feed.objects(url__iexact=url)
        if fds.count() > 0:
            fd = fds.first()
        else:
            try:
                parsed_fd = feedparser.parse(url)
                logger.info(parsed_fd)
                fd = Feed(title=parsed_fd.feed.title, url=url, if_valid=True)
                fd.save()

                if fd.id not in channel.feed_list:
                    channel.feed_list.append(fd.id)
                    channel.save()

            except Exception as e:
                logger.error('Feed not valid? Recheck and add again.')
                logger.error(e.message)
                return None

            # FeedController.process(fd.id, channel.id)
        return fd

    @staticmethod
    def process_article(entry, channel, feed, parsed_feed):
        """ process the article and generated entities """

        try:
            res = Extractor().process(entry.link)
        except Exception as e:
            logger.error(e.message)
            return # for now, skip the article if parsing fails

        article = res['article_object']
        scored_entities = res['scored_entities']

        if hasattr(entry, 'author'):
            author_name = entry.author
        else:
            if len(article.authors) > 0:
                logger.info(article.authors)
                author_name = ', '.join(article.authors)
            else:
                author_name = 'NA'

        author = AuthorController.add(name=author_name, current_publication=parsed_feed.feed.title)
        if author.id not in channel.author_list:
            channel.author_list.append(author.id)
            channel.save()

        if channel.id not in author.channels:
            author.channels.append(channel.id)
            author.save()

        new_art_list = Article.objects(url=article.url)
        if new_art_list.count() == 0:
            new_art = Article(url=article.url, feed_id=feed.id, title=article.title,
                              summary=article.summary, top_image=article.top_image, author=author.id,
                              publish_date=article.publish_date)
            new_art.save()
            author.articles.append(new_art.id)
            author.save()

            logger.info(new_art)

            for en in scored_entities:
                en_obj_q = Entity.objects(title=en, channel_id=channel.id)
                if en_obj_q.count() == 0:
                    en_obj = Entity(title=en, score=scored_entities[en], channel_id=channel.id)
                else:
                    en_obj = en_obj_q.first()
                    en_obj.score = en_obj.score + scored_entities[en]

                en_obj.save()

                new_art.entity_list.append(en_obj.id)
                new_art.save()

                author.entities.append(en_obj.id)
                author.save()

                channel.entity_list.append(en_obj.id)
                channel.save()

                logger.info(en_obj)
        else:
            new_art = new_art_list.first()
            logger.info("Article already processed before")


    @staticmethod
    def process(feed_id, channel_id):
        """ parse the feed """

        channel = Channel.objects(id=channel_id).first()
        feed = Feed.objects(id=feed_id).first()
        logger.info("Parsing url %s" % feed.url)

        try:
            fd = feedparser.parse(feed.url)
        except Exception as e:
            logger.exception(e.message)
            logger.error('Failed parsing feed %s', feed.url)
            return

        for entry in fd.entries:
            FeedController.process_article(entry, channel, feed, fd)
