import nltk
from newspaper import Article
from collections import Counter
import tldextract
import feedparser
import logging


logger = logging.getLogger(__name__)


class Extractor:

    def __extract_article(self, url):
        """ extracts text from article """

        article = Article(url)
        article.download()
        article.parse()
        article.nlp()
        return article

    def __get_domain(self, url):
        """ gets the domain from the article url """

        dext = tldextract.extract(url)
        return {
            'domain': dext.domain,
            'subdomain': dext.subdomain,
            'suffix': dext.suffix
        }

    def __extract_entity_names(self, t):
        entity_names = []

        if hasattr(t, 'label') and t.label:
            if t.label() == 'NE':
                entity_names.append(' '.join([child[0] for child in t]))
            else:
                for child in t:
                    entity_names.extend(self.__extract_entity_names(child))

        return entity_names

    def process(self, url):
        """
        takes care of parsing article, extracting entities, and returns
        found article object and entities
        """
        article = self.__extract_article(url)
        sentences = nltk.sent_tokenize(article.text)
        tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
        tagged_sentences = [nltk.pos_tag(sentence) for sentence in tokenized_sentences]
        chunked_sentences = nltk.ne_chunk_sents(tagged_sentences, binary=True)

        entity_names = []
        for tree in chunked_sentences:
            entity_names.extend(self.__extract_entity_names(tree))

        scored_entities = dict(Counter(entity_names))
        domain_info = self.__get_domain(url)

        return {'article_object': article, 'scored_entities': scored_entities, 'domain_info': domain_info}

    def process_feed(self, feed_url):
        """ build full feed intelligence """

        domain_hash = entities = {}
        categories = []
        authors = []
        subdomains = []
        articles = []

        ys = feedparser.parse(feed_url)
        for art in ys.entries:
            print art.link
            res = self.process(art.link)
            entities.update(res['scored_entities'])

            if hasattr(art, 'author'):
                author_name = art.author
            else:
                if len(res['article_object']['authors']) > 0:

                    logger.info(res['article_object']['authors'])
                    author_name = ', '.join(res['article_object']['authors'])
                else:
                    author_name = 'NA'

            authors.append(author_name)
            subdomains.append(res['domain_info']['subdomain'])
            categories.append(art.category)
            articles.append({
                'url': res['article_object'].url,
                'title': res['article_object'].title,
                'author': art.author,
                'entities': res['scored_entities'],
                'publish_date': res['article_object'].publish_date,
                'category': art.category
            })

        return {
            'authors': authors,
            'subdomains': subdomains,
            'entities': entities,
            'categories': categories,
            'articles': articles,
        }
