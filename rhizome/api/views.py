import logging
import json

from django.shortcuts import render
from django.contrib.auth import login, logout

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import  AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from serializers import LoginSerializer, TokenSerializer, PasswordChangeSerializer
from utils import get_request_content, get_user_customer_from_token

class LoginView(GenericAPIView):

    """

    Check the credentials and return the REST Token of the user
    if the credentials are valid and authenticated.
    Calls Django Auth login method to register User ID
    in Django session framework.

    Accept the following POST parameters: ---->email, password
    Return : ------> REST Framework Token Object's key.

    """
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer
    token_model = Token
    response_serializer = TokenSerializer

    def login(self):

        self.user = self.serializer.validated_data['user']
        self.token, created = self.token_model.objects.get_or_create(
            user=self.user)
        login(self.request, self.user) # calling Django auth login method to register user ID in django session framework

    def get_response(self):

        return Response(
            self.response_serializer(self.token).data, status=status.HTTP_200_OK
        )

    def get_error_response(self):

        return Response(
            self.serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )
    


    def post(self, request, *args, **kwargs):

        """
        This method will check the validation method that are in serializers
        """

        self.serializer = self.get_serializer(data=self.request.data)
        if not self.serializer.is_valid():
            return self.get_error_response()
        self.login()
        return self.get_response()



class LogoutView(APIView):

    """
    Calls Django logout method and delete the Token object assigned to the current User object.
    Accepts/Returns nothing.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


    def post(self, request):

        try:

            request.user.auth_token.delete()
        except:
            pass

        logout(request) #  calling Django Auth logout method

        return Response({"success": "Successfully logged out."},
                        status=status.HTTP_200_OK)


class PasswordChangeView(GenericAPIView):

    """
    Calls Django Auth SetPasswordForm save method.
    Accepts the following POST parameters :----> old_password, new_password1, new_password2
    Returns the success/fail message.
    """
    authentication_classes = (TokenAuthentication,)
    serializer_class = PasswordChangeSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        serializer = self.get_serializer(data=request.data)
        print serializer
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response({"success": "New password has been saved."})











