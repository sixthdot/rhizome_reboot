import json
import logging

from django.conf import settings
from django.utils.encoding import force_bytes
from django.core.urlresolvers import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm

from rest_framework import serializers, exceptions
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from rest_framework import serializers
try:
    from django.utils.http import urlsafe_base64_decode as uid_decoder
except:
    # make compatible with django 1.5
    from django.utils.http import base36_to_int as uid_decoder

logger = logging.getLogger(__name__)

'''
    `LoginSerializer` serializer will be used in login process, 
    Checks these validation

    1. if email is invalid  

    Response  --->  {
                        "email": [
                            "Enter a valid email address."
                        ]
                    }

    2. if email or password field is empty.

    Response --->  {
                        "field": [
                            "This field may not be blank."
                        ]
                    }
    `field` key can be email or password .

    3. if email and password didn't match.

    Response --> {
                    "non_field_errors": [
                        "Unable to log in with provided credentials."
                    ]
                }

    
    4. if user is not active user

    Response --->   {
                        "non_field_errors": [
                            "User account is disabled."
                        ]
                }
    5. if user is authenticated user,

    will return this response

    return
    {
        "key": "76efa9d581281c94ab8607a51d4f3237cdc583df"
    }

    key's value is Token of authenticated user

'''


class LoginSerializer(serializers.Serializer):
    
    email = serializers.EmailField(required=True, allow_blank=False)
    password = serializers.CharField(style={'input_type': 'password'}, required=True, allow_blank=False)

    def validate(self, attrs):

        email = attrs.get('email')
        password = attrs.get('password')
        
        if email and password:
            
            logger.debug(' calling authenticate.......')
            user = authenticate(username=email, password=password)

        else:

            msg = _('Must include "email" and "password".')
            raise exceptions.ValidationError(msg)
            # Authentication through username

        # Did we get back an active user?
        if user:

            if not user.is_active:

                msg = _('User account is disabled.')
                raise exceptions.ValidationError(msg)

        else:

            msg = _('Unable to log in with provided credentials.')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs



class TokenSerializer(serializers.ModelSerializer):
    """
    Serializer for Token model.
    """

    class Meta:
        model = Token
        fields = ('key',) 


'''
    `PasswordResetSerializer` is used in the process of PasswordReset process

1. if email field is empty

Response ------>{
                    "email":["This field may not be blank."]
                }

2. if email address is not valid

Resposne ------>{
                    "email":["Enter a valid email address."]
                }


3. if email-address is not associated with our database

Response ------>{
                    "email":["This email address doesn't have an associated user account."]
                }


4. if email address associated with inactive user..

Resposne ----->{
                    "email": ["This email address have an associated inactive-user account."]
                }


5. if successfully sent mail 

Resposne ------>{
                    "success":"Password reset e-mail has been sent."
                }


class PasswordResetSerializer(serializers.Serializer):

    """
    Serializer for requesting a password reset e-mail.
    """

    email = serializers.EmailField(required=True)

    password_reset_form_class = PasswordResetForm


    def validate_email(self, value):
        # Create PasswordResetForm with the serializer

        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError('Error')
        else:
            logger.debug('calling clean email function')
            self.clean_email()    
        return value

    def clean_email(self):
        """
        Validates that an active user exists with the given email address.
        """

        request = self.context.get('request')
        email = request.data['email']
        user = get_user_model()

        try:

            is_user_exist = user._default_manager.get(email__iexact=email)

        except ObjectDoesNotExist:
            logger.debug('Object does not exists in our database:'+email)
            raise serializers.ValidationError("This email address doesn't have an associated user account.")
        else:
            if not is_user_exist.is_active:
                # none of the filtered users are active
                raise serializers.ValidationError("This email address have an associated inactive-user account.")
        
        return email 

    def save(self):

        request = self.context.get('request')
        logger.debug(request)
        token_generator = default_token_generator
        token = token_generator.make_token(request.user)
        email = request.data['email']
        uid = urlsafe_base64_encode(force_bytes(request.user.pk))
        # reverse_url = reverse('mobile_url:password_reset')
        if request.is_secure():
            protocol = 'https'
        else:
            protocol = 'http'
        url = reverse('mobile_url:password_reset', kwargs= {'uid':uid, 'token':token})
        text_to_be_sent = "Dear "+request.user.first_name+"\n\nWe have just received a request from you for resetting your xyz account password.\nKindly Reset your password using the following link: \n\n"+ protocol+"://www.xyz.com"+url+"\n\nThanks\nxyz Team\nxyz.com\n"
        email_info_to_be_sent = {'from_email': 'support@pillofy.com',
                        'from_name': 'Pillofy',
                        'signing_domain': 'pillofy.com',
                        'subject': 'Password Reset',
                        'text': text_to_be_sent,
                        'to': [{'email': email,
                                 'name': request.user.first_name,
                                 'type': 'to'}],
                        }
        send_mail(email_info_to_be_sent)

can't be implemented until mailgun is integrated
'''

'''

--------PasswordChangeSerializer is used in the process of change password process----

Note---> For saving password, it is using  `SetPasswordForm` class method

def validate_old_password() method will check old password by using `check_password` method of `AbstractBaseUser`
if it return False

Response -->{
                "old_password":["Invalid Old password"]
            }


def validate() method will check new_password1 and new_password2 fields.

1. if new_password1 or new_password2 is empty 

   Response --> {
                    "new_password2":["This field may not be blank."]
                }         

2. if new_password1 and new_password2 are not equal

    Response -->{
                    "new_password2":["The two password fields didn't match."]
                }

3. if password change successfully 

    Response -->{
                    "success":"New password has been saved."
                }

'''        


class PasswordChangeSerializer(serializers.Serializer):

    old_password = serializers.CharField(max_length=128, style={'input_type': 'password'},required=True, allow_blank=False)
    new_password1 = serializers.CharField(max_length=128, style={'input_type': 'password'}, required=True, allow_blank=False)
    new_password2 = serializers.CharField(max_length=128, style={'input_type': 'password'}, required=True, allow_blank=False)

    set_password_form_class = SetPasswordForm

    def __init__(self, *args, **kwargs):
        super(PasswordChangeSerializer, self).__init__(*args, **kwargs)

        self.request = self.context.get('request')
        self.user = getattr(self.request, 'user', None)

    def validate_old_password(self, value):

        """
        will validate user's old password by using `check_password` method of `AbstractBaseUser`

        """

        invalid_password_conditions = (
        
            self.user,
            not self.user.check_password(value)
        )

        if all(invalid_password_conditions):

            raise serializers.ValidationError('Invalid Old password')

        return value

    def validate(self, attrs):

        self.set_password_form = self.set_password_form_class(
            user=self.user, data=attrs
        )

        if not self.set_password_form.is_valid():
            raise serializers.ValidationError(self.set_password_form.errors)
        return attrs

    def save(self):
        self.set_password_form.save()

