from django.conf import settings
from django.contrib import admin
from django.conf.urls import include, url
from django.conf.urls.static import static

from rest_framework.urlpatterns import format_suffix_patterns
import views as api_views

from api.views import LoginView, LogoutView, PasswordChangeView

urlpatterns = [

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^password/change/$', PasswordChangeView.as_view()),

]