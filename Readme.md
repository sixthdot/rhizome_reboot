## How to install the stack to contribute to  rhizome dev on local machine

### Step 1: Install dependencies

1. Mongo (this stores the storyboard json)
2. Redis (this is used as a queue for async tasks)
3. MySQL (this is the primary customer and billing detail db)
4. Python and pip (primary codebase in the backend)
5. Virtualenv ```pip install virtualenv```

Please install the above in either a Vagrant VM or a Virtualbox VM, or on your core system.

### Step 2: Clone the repo

```shell
git clone git@gitlab.com:hsntech/rhizome.git
```

### Step 3: Install python dependencies

1. Create a virtualenv folder: ```mkdir envs```
2. Initialize virtualenv: ```cd envs; virtualenv sixthdot_env```
3. Activate virtualenv: ```source sixthdot_env/bin/activate```
4. Go to your cloned repo directory (from same shell which has virtualenv activated): ```cd workspace/pr47-django```
5. Run ```xcode-select --install```
6. Run ```pip install lxml```
7. Pip install requirements: ```pip install -r requirements.txt``` (go and enjoy coffee for a bit)
8. install nltk and its resources by running the following in python shell:
```import nktl``` 
```nktl.download()```
then install the following packages
-- corpora/words
-- chunkers/maxent_ne_chunker
-- tokenizers/punkt
-- taggers/maxent_treebank_pos_tagger
9. Run ```pip install lxml==3.3.5 newspaper==0.0.9.8 nltk==3.0.1 numpy==1.11.0```


### Step 4: Setup the settings_local

You can overwrite parts of settings.py by simply providing a settings_local.py in rhizome/rhizome/ folder.
This is needed so you can have the local db credentials and other such details specific to your
environment.

```
cd rhizome/rhizome
cp settings_local.py.sample settings_local.py
```

Now change the settings in settings_local.py (like the db name, user, password, IP of the machines, allowed hosts etc.)

### Step 5: Setup the DB

You would need to create a database in MySQL db, with the settings you provided in settings_local.py.
Once that is done, let's create the tables:

```
python manage.py migrate
python manage.py createsuperuser
```

### Step 6: Done!

You can now run the stack.

```
python manage.py runserver_plus
```

Head to ```http://localhost:8000/admin/```
